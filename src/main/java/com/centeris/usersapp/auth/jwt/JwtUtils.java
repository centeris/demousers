package com.centeris.usersapp.auth.jwt;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class JwtUtils {
    private static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    @Value("${users.app.jwtSecret}")
    private String jwtSecret;

    @Value("${users.app.jwtExpirationMs}")
    private int jwtExpirationMs;

    private Claims claims = null;

    public String generateJwtToken(Authentication authentication) throws JsonProcessingException {

        User userPrincipal = (User) authentication.getPrincipal();
        var roles = authentication.getAuthorities();
        boolean isAdmin = roles.stream().anyMatch(r -> r.getAuthority().contains("ADMIN"));
        Map<String, Object> claims = new HashMap<>();
        claims.put("authorities", roles);
        claims.put("admin", isAdmin);
        claims.put("username", userPrincipal.getUsername());

        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .addClaims(claims)
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(key(), SignatureAlgorithm.HS256)
                .compact();
    }

    private Key key() {
        return Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecret));
    }

    public String getUserNameFromJwtToken(String token) {
        if (claims == null) {
            claims = Jwts.parserBuilder().setSigningKey(key()).build().parseClaimsJws(token).getBody();
        }
        return claims.getSubject();
    }

    public List<SimpleGrantedAuthority> getRolesFromJwtToken(String token) {
        if (claims == null) {
            claims = Jwts.parserBuilder().setSigningKey(key()).build().parseClaimsJws(token).getBody();
        }
        var authorities = claims.get("authorities", ArrayList.class);

        return (List<SimpleGrantedAuthority>) authorities.stream()
                .map(grantedAuthority -> new SimpleGrantedAuthority(((LinkedHashMap<?, ?>) grantedAuthority).get("authority").toString()))
                .collect(Collectors.toList());
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parserBuilder().setSigningKey(key()).build().parse(authToken);
            return true;
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            logger.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty: {}", e.getMessage());
        }

        return false;
    }

    public int getJwtExpirationMs() {
        return jwtExpirationMs;
    }
}