package com.centeris.usersapp.controllers;

import com.centeris.usersapp.models.dto.UserDTO;
import com.centeris.usersapp.models.entities.User;
import com.centeris.usersapp.models.request.UserRequest;
import com.centeris.usersapp.services.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:5173")
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("")
    public List<UserDTO> findAll() {
        return userService.findAll();
    }

    @GetMapping("/page/{page}")
    public Page<UserDTO> findAll(@PathVariable int page) {
        Pageable pageable = PageRequest.of(page, 1);
        return userService.findAll(pageable);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> show(@PathVariable Long id) {
        Optional<UserDTO> userOptional = userService.findById(id);

        if (userOptional.isPresent()) {
            return ResponseEntity.ok(userOptional.orElseThrow());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("")
    public ResponseEntity<?> create(@Valid @RequestBody User user, BindingResult result) {
        if (result.hasErrors()) {
            return validation(result);
        }

        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(userService.store(user));
        } catch (Exception e) {
            return validationSave(e);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@Valid @RequestBody UserRequest user, BindingResult result, @PathVariable Long id) {
        if (result.hasErrors()) {
            return validation(result);
        }

        try {
            Optional<UserDTO> userOptional = userService.update(user, id);

            if (userOptional.isPresent()) {
                return ResponseEntity.status(HttpStatus.CREATED).body(userOptional.orElseThrow());
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return validationSave(e);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        Optional<UserDTO> userOptional = userService.findById(id);

        if (userOptional.isPresent()) {
            userService.deleteById(id);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

    private ResponseEntity<?> validation(BindingResult result) {
        Map<String, String> errors = new HashMap<>();

        result.getFieldErrors().forEach(error -> {
            errors.put(error.getField(), "El campo " + error.getField() + " " + error.getDefaultMessage());
        });

        return ResponseEntity.badRequest().body(errors);
    }

    private ResponseEntity<?> validationSave(Exception e) {
        Map<String, String> errors = new HashMap<>();

        if (e.getMessage().contains("UK_username")) {
            errors.put("username", "Ya existe un usuario con ese username");
        }

        if (e.getMessage().contains("UK_email")) {
            errors.put("email", "Ya existe un usuario con ese correo");
        }

        return ResponseEntity.badRequest().body(errors);
    }
}
