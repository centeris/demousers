package com.centeris.usersapp.models.dto.mapper;

import com.centeris.usersapp.models.dto.UserDTO;
import com.centeris.usersapp.models.entities.User;

public class DTOMapperUser {

    private User user;

    private DTOMapperUser() {
    }

    public static DTOMapperUser builder() {
        return new DTOMapperUser();
    }

    public DTOMapperUser setUser(User user) {
        this.user = user;
        return this;
    }

    public UserDTO build() {
        if (user == null) {
            throw new RuntimeException("Debe asignar un Usuario");
        }
        boolean isAdmin = user.getRoles().stream().anyMatch(it -> it.getName().equals("ROLE_ADMIN"));
        return new UserDTO(user.getId(), user.getUsername(), user.getEmail(), isAdmin);
    }
}
