package com.centeris.usersapp.models;

public interface IUser {

    boolean isAdmin();
}
