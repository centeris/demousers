package com.centeris.usersapp.services;

import com.centeris.usersapp.models.IUser;
import com.centeris.usersapp.models.dto.UserDTO;
import com.centeris.usersapp.models.dto.mapper.DTOMapperUser;
import com.centeris.usersapp.models.entities.Role;
import com.centeris.usersapp.models.entities.User;
import com.centeris.usersapp.models.request.UserRequest;
import com.centeris.usersapp.repositories.RoleRepository;
import com.centeris.usersapp.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Override
    @Transactional(readOnly = true)
    public Page<UserDTO> findAll(Pageable pageable) {
        return userRepository.findAll(pageable).map(this::userToUserDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<UserDTO> findAll() {
        return ((List<User>) userRepository.findAll())
                .stream().map(this::userToUserDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserDTO> findById(Long id) {
        return userRepository.findById(id).map(this::userToUserDto);
    }

    @Override
    @Transactional
    public UserDTO store(User user) {
        user.setPassword(encoder.encode(user.getPassword()));

        user.setRoles(getRoles(user));

        return userToUserDto(userRepository.save(user));
    }

    @Override
    @Transactional
    public Optional<UserDTO> update(UserRequest user, Long id) {
        Optional<User> userOptional = userRepository.findById(id);

        if (userOptional.isPresent()) {
            User userDb = userOptional.orElseThrow();
            userDb.setEmail(user.getEmail());
            userDb.setUsername(user.getUsername());

            userDb.setRoles(getRoles(user));

            return Optional.of(
                    userToUserDto(userRepository.save(userDb))
            );
        }
        return Optional.empty();
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        userRepository.deleteById(id);
    }

    private UserDTO userToUserDto(User user) {
        return DTOMapperUser.builder().setUser(userRepository.save(user)).build();
    }

    private List<Role> getRoles(IUser user) {
        List<Role> roles = new ArrayList<>();
        var role = roleRepository.findByName("ROLE_USER");

        if (role.isPresent()) {
            roles.add(role.orElseThrow());
        }

        if (user.isAdmin()) {
            role = roleRepository.findByName("ROLE_ADMIN");

            if (role.isPresent()) {
                roles.add(role.orElseThrow());
            }
        }

        return roles;
    }

}
