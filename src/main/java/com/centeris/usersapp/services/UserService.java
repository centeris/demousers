package com.centeris.usersapp.services;

import com.centeris.usersapp.models.dto.UserDTO;
import com.centeris.usersapp.models.entities.User;
import com.centeris.usersapp.models.request.UserRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<UserDTO> findAll();

    Page<UserDTO> findAll(Pageable pageable);

    Optional<UserDTO> findById(Long id);

    UserDTO store(User user);

    Optional<UserDTO> update(UserRequest user, Long id);

    void deleteById(Long id);

}
